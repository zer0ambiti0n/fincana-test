<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use ZeroAmbition\FincanaTest\Dto\CreateOrderDto;

class CreateOrderDtoTest extends TestCase
{
    public function testItMustToArray(): void
    {
        $arrayData = [
            'pair' => 'some_pair',
            'type' => 'some_type',
            'action' => 'some_action',
            'amount' => 10,
            'price' => 0.08,
        ];

        $dto = new CreateOrderDto(...$arrayData);

        array_walk($arrayData, fn ($value, $key) => $this->assertEquals($dto->$key, $value));

        $arrayDto = $dto->toArray();
        $this->assertEqualsCanonicalizing($arrayData, $arrayDto);
    }
}
