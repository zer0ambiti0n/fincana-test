<?php
//Истанцируем сервис, передав нужные параметры
$payeerService = new \ZeroAmbition\FincanaTest\TradePayeerService(
    userId: getenv('TRADE_PAYEER_USERID'),
    apiSecret:  getenv('TRADE_PAYEER_API_SECRET'),
);

$info = $payeerService->getInfo('BTC_USD');

$orders = $payeerService->getOrders();

$myAccount = $payeerService->getAccount();

$createdOrder = $payeerService->createOrder(new \ZeroAmbition\FincanaTest\Dto\CreateOrderDto(
    pair: 'BTC_USD',
    type: 'limit',
    action: 'buy',
    amount: 10,
    price:  0.08
));

$orderStatus = $payeerService->getOrderStatus($createdOrder['id']);

$myOrders = $payeerService->getMyOrders();
