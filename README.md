## FINCANA TEST

Run tests:
`composer test`

Lint:
`composer codestyle:fix`

See [examples here](examples/example.php)
