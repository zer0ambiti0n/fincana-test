<?php

namespace ZeroAmbition\FincanaTest;

use CurlHandle;
use ZeroAmbition\FincanaTest\Exceptions\ApiException;

class TradePayeerApiClient
{
    private array $errors = [];

    /**
     * TradePayeerApiClient constructor
     *
     * @param string $userId
     * @param string $apiSecret
     * @param string $baseUri
     */
    public function __construct(
        private readonly string $userId,
        private readonly string $apiSecret,
        private readonly string $baseUri = 'https://payeer.com/api/trade/'
    ) {
    }

    /**
     * Make api request
     *
     * @param string $path
     * @param array  $params
     *
     * @return array
     * @throws ApiException
     */
    public function request(string $path, array $params = []): array
    {
        $params = [
            'post' => $params,
            'ts' => round(microtime(true) * 1000)
        ];
        $requestBody = json_encode($params);

        $curl = $this->setApiSign($this->prepareCurl(), $path, $requestBody);

        curl_setopt($curl, CURLOPT_URL, $this->baseUri.$path);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $requestBody);

        $jsonResponse = curl_exec($curl);
        curl_close($curl);

        $arResponse = json_decode($jsonResponse, true);

        if ($arResponse['success'] !== true) {
            $this->errors = $arResponse['error'];
            throw new ApiException($arResponse['error']['message']);
        }

        return $arResponse;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    private function prepareCurl(): CurlHandle
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            "Content-Type: application/json",
            "API-ID: ".$this->userId,
        ]);

        return $curl;
    }

    private function setApiSign(CurlHandle $curl, string $path, string $requestBody): CurlHandle
    {
        $sign = hash_hmac('sha256', $path.$requestBody, $this->apiSecret);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ["API-SIGN: ".$sign]);

        return $curl;
    }
}
