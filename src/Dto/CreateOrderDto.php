<?php

namespace ZeroAmbition\FincanaTest\Dto;

use ZeroAmbition\FincanaTest\Traits\Arrayable;

class CreateOrderDto
{
    use Arrayable;

    public function __construct(
        public readonly string $pair,
        public readonly string $type,
        public readonly string $action,
        public readonly int $amount,
        public readonly float $price,
    ) {
    }
}
