<?php

namespace ZeroAmbition\FincanaTest\Constants;

class ErrorCodes
{
    public const INVALID_SIGNATURE = 'INVALID_SIGNATURE';
    public const INVALID_IP_ADDRESS = 'INVALID_IP_ADDRESS';
    public const LIMIT_EXCEEDED = 'LIMIT_EXCEEDED';
    public const INVALID_TIMESTAMP = 'INVALID_TIMESTAMP';
    public const ACCESS_DENIED = 'ACCESS_DENIED';
    public const INVALID_PARAMETER = 'INVALID_PARAMETER';
    public const PARAMETER_EMPTY = 'PARAMETER_EMPTY';
    public const INVALID_STATUS_FOR_REFUND = 'INVALID_STATUS_FOR_REFUND';
    public const REFUND_LIMIT = 'REFUND_LIMIT';
    public const UNKNOWN_ERROR = 'UNKNOWN_ERROR';
}
