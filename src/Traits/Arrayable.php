<?php

namespace ZeroAmbition\FincanaTest\Traits;

use ReflectionClass;
use ReflectionProperty;

trait Arrayable
{
    /**
     * Returns associative array as ['publicPropName' => propValue]
     *
     * @return array
     */
    public function toArray(): array
    {
        $reflection = new ReflectionClass(static::class);

        $array = [];

        foreach ($reflection->getProperties(ReflectionProperty::IS_PUBLIC) as $reflectionProperty){
            $property = $reflectionProperty->getName();
            $array[$property] = $this->$property;
        }

        return $array;
    }
}