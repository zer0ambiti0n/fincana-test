<?php

namespace ZeroAmbition\FincanaTest\Exceptions;

use Exception;
use Throwable;
use ZeroAmbition\FincanaTest\Constants\ErrorCodes;

/**
 * Class TradePayeerApiClientException
 */
class ApiException extends Exception
{
    public const MAP_MESSAGES = [
        ErrorCodes::ACCESS_DENIED => 'Доступ к API/ордеру запрещен. Проверьте orderId',
        ErrorCodes::INVALID_IP_ADDRESS =>
            'IP-адрес ip источника запроса не совпадает с тем, который прописан в настройках API',
        ErrorCodes::INVALID_PARAMETER => 'Параметр parameter указан неверно',
        ErrorCodes::INVALID_SIGNATURE =>
            'Возможные причины: - неверная подпись API-SIGN - 
            неверно указан API-ID- API-пользователь заблокирован (можно разблокировать в настройках)',
        ErrorCodes::INVALID_TIMESTAMP => 'Параметр ts указан неверно',
        ErrorCodes::INVALID_STATUS_FOR_REFUND =>
            'Cтатус status ордера не позволяет произвести возврат (ордер уже возвращен или отменен)',
        ErrorCodes::LIMIT_EXCEEDED =>
            'Превышение установленных лимитов (количество запросов/весов/ордеров), подробнее указано в параметре desc',
        ErrorCodes::REFUND_LIMIT => 'Ордер может быть отменен не менее через 1 минуту после создания',
        ErrorCodes::PARAMETER_EMPTY => 'Параметр parameter обязателен (не должен быть пустым)',
        ErrorCodes::UNKNOWN_ERROR =>
            'Неизвестная ошибка на бирже. Торги приостановлены для проверки. Попробуйте через 15 минут.'
    ];

    public function __construct(string $message = "", int $code = 0, ?Throwable $previous = null)
    {
        $message = self::MAP_MESSAGES[$message] ?? "UnknownApiError";
        parent::__construct($message, $code, $previous);
    }
}
