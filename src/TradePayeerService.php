<?php

namespace ZeroAmbition\FincanaTest;

use ZeroAmbition\FincanaTest\Dto\CreateOrderDto;
use ZeroAmbition\FincanaTest\Exceptions\ApiException;

class TradePayeerService
{
    private TradePayeerApiClient $apiClient;

    /**
     * TradePayeerService constructor
     *
     * @param string $userId
     * @param string $apiSecret
     */
    public function __construct(string $userId, string $apiSecret)
    {
        $this->apiClient = new TradePayeerApiClient(
            userID: $userId,
            apiSecret: $apiSecret
        );
    }

    /**
     * Get limits, available pairs and its` params
     *
     * @param string|null $pair
     *
     * @return array
     * @throws ApiException
     */
    public function getInfo(?string $pair = null): array
    {
        return $pair ?
            $this->apiClient->request('info', ['pair' => $pair]) : $this->apiClient->request('info');
    }

    /**
     * Get available orders by pair
     *
     * @param string|null $pair
     *
     * @return array
     * @throws ApiException
     */
    public function getOrders(?string $pair = 'BTC_USD'): array
    {
        return $this->apiClient->request('orders', ['pair' => $pair]);
    }

    /**
     * Get user`s balance
     *
     * @return array
     * @throws ApiException
     */
    public function getAccount(): array
    {
        return $this->apiClient->request('account');
    }

    /**
     * Create order by CreateOrderDto
     *
     * @param CreateOrderDto $dto
     *
     * @return array
     * @throws ApiException
     */
    public function createOrder(CreateOrderDto $dto): array
    {
        return $this->apiClient->request('order_create', $dto->toArray());
    }

    /**
     * Get order status by ID
     *
     * @param string $orderId
     *
     * @return array
     * @throws ApiException
     */
    public function getOrderStatus(string $orderId): array
    {
        return $this->apiClient->request('order_status', ['order_id' => $orderId]);
    }

    /**
     * Get user`s order by filters
     *
     * @param string|null $pair
     * @param string|null $action
     *
     * @return array
     * @throws ApiException
     */
    public function getMyOrders(?string $pair = null, ?string $action = null): array
    {
        $filters = array_merge(
            $pair ? ['pair' => $pair] : [],
            $action ? ['action' => $action] : []
        );

        return !empty($filters) ?
            $this->apiClient->request('my_orders', $filters) : $this->apiClient->request('my_orders');
    }
}
